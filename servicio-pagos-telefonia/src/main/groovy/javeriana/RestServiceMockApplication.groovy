package javeriana

import org.h2.server.web.WebServlet
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.data.repository.CrudRepository
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@EnableEurekaClient
@SpringBootApplication
class RestServiceMockApplication implements CommandLineRunner {

	@Autowired
	private PaymentRequestRepository repository;

	static void main(String[] args) {
		SpringApplication.run RestServiceMockApplication, args
	}

	@Override
	void run(String... args) throws Exception {
		for (int i = 0; i < 10; i++) {
			repository.save(new PaymentRequest((new Random().nextInt(8) + 1) * 1000))
		}
	}

	@Bean
	ServletRegistrationBean h2servletRegistration(){
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet())
		registrationBean.addUrlMappings("/console/*")
		return registrationBean
	}
}

@Entity
class PaymentRequest {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long paymentReference

	private Long value

	PaymentRequest() {}
	PaymentRequest(Long value) {this.value = value}

	Long getPaymentReference() { return paymentReference }
	void setPaymentReference(Long paymentReference) { this.paymentReference = paymentReference }
	Long getValue() { return value }
	void setValue(Long value) { this.value = value }
}

interface PaymentRequestRepository extends CrudRepository<PaymentRequest, Long> {
}

@RestController
@RequestMapping("/payment")
class PaymentController {

	@Autowired
	private PaymentRequestRepository repository;

	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	List<PaymentRequest> getAll() {
		return repository.findAll()
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity get(@PathVariable Long id) {
		System.out.println("(get) invoked with: " + id)
		PaymentRequest paymentRequest = repository.findOne(id)
		if (paymentRequest != null) {
			return new ResponseEntity(paymentRequest, HttpStatus.OK)
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND)
		}
	}

	@PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity processPayment(@RequestBody PaymentRequest request) {
		System.out.println("(post) invoked with: " + request.getPaymentReference() + request.getValue())
		if (repository.findOne(request.getPaymentReference()) != null) {
			return new ResponseEntity(HttpStatus.OK)
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND)
		}
	}

	@DeleteMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity compensatePayment(@RequestBody PaymentRequest request) {
		System.out.println("(delete) invoked with: " + request.getPaymentReference() + request.getValue())
		if (repository.findOne(request.getPaymentReference()) != null) {
			return new ResponseEntity(HttpStatus.ACCEPTED)
		} else {
			return new ResponseEntity(HttpStatus.NOT_FOUND)
		}
	}
}



