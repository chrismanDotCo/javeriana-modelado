package javeriana;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by chrismac on 5/23/17.
 */
public interface PaymentRepository extends CrudRepository<Payment, Long> {
}
