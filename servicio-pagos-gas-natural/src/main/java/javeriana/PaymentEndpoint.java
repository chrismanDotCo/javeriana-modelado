package javeriana;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PaymentEndpoint {
	private static final String NAMESPACE_URI = "http://someuri";

	@Autowired
	private PaymentRepository repository;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "paymentRequest")
	@ResponsePayload
	public PaymentResponse processPayment(@RequestPayload PaymentRequest request) {
		System.out.println("(payment) invoked with: " + request.getPaymentReference() + " " + request.getValue());
		PaymentResponse response = new PaymentResponse();
		response.setResponse("OK");

		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "infoRequest")
	@ResponsePayload
	public InfoResponse getInfo(@RequestPayload InfoRequest request) {
		System.out.println("(payment) invoked with: " + request.getPaymentReference());
		Payment payment = repository.findOne(request.getPaymentReference());

		InfoResponse response = new InfoResponse();
		response.setValue(payment != null? payment.getValue().longValue(): -1);

		return response;
	}
}
