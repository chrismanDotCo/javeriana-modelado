# JAVERIANA - MODELADO Y VALIDACION #


**How to make it work:**


* gradlew build
* cd docker/
* docker-compose up


**How to generate a token:**

curl -X POST http://localhost:9191/uaa/oauth/token -H 'accept: application/json' -H 'authorization: Basic amF2ZXJpYW5hOm1vZGVsYWRv' -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' -F username=user1 -F password=pass1 -F grant_type=password -F scope=taller


**How to request a process payment:**

curl -X POST http://localhost:8888/pagos -H 'authorization: BEARER 878c922f-fa37-43bf-8e8d-da97c79a06d0' -H 'content-type: application/json' -d '{"service": "GAS", "reference": "9"}'


**How to request a process compensation:**

curl -X DELETE http://localhost:8888/pagos -H 'authorization: BEARER 878c922f-fa37-43bf-8e8d-da97c79a06d0' -H 'content-type: application/json' -d '{"service": "GAS", "reference": "9"}'