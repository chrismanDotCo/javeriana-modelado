package co.chrisman

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@EnableEurekaServer
@SpringBootApplication
class ServiceRegistryApplication

fun main(args: Array<String>) {
    SpringApplication.run(ServiceRegistryApplication::class.java, *args)
}
