package co.chrisman;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by chrismac on 5/23/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Savings {

    @Id
    private String user;

    private Long value;
}