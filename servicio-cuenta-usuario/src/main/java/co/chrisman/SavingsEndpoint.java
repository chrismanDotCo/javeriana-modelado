package co.chrisman;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class SavingsEndpoint {
	private static final String NAMESPACE_URI = "http://someuri";

	@Autowired
	private SavingsRepository repository;


	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "savingsRequest")
	@ResponsePayload
	public SavingsResponse getSavings(@RequestPayload SavingsRequest request) {
		Savings savings = repository.findOne(request.getUser());

		SavingsResponse response = new SavingsResponse();
		response.setValue(savings != null? savings.getValue(): -1);

		return response;
	}
}
