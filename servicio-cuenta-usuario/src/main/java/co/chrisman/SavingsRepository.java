package co.chrisman;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by chrismac on 5/23/17.
 */
public interface SavingsRepository extends CrudRepository<Savings, String> {
}
