package javeriana;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	@Autowired
	private MyEmailSender emailSender;

	@Test
	public void contextLoads() {
		emailSender.sendMail("contrerasc.javeriana@gmail.com",
				"Compensacion factura",
				"Compensacion factura");
	}

}
