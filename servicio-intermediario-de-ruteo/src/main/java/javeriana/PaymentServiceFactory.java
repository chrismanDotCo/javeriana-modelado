package javeriana;

import javeriana.agua.AguaServiceClient;
import javeriana.energia.EnergiaServiceClient;
import javeriana.gas.GasServiceClient;
import javeriana.telefonia.TelefoniaServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by chrismac on 5/23/17.
 */
@Component
public class PaymentServiceFactory {

    @Autowired
    AguaServiceClient aguaServiceClient;

    @Autowired
    GasServiceClient gasServiceClient;

    @Autowired
    TelefoniaServiceClient telefoniaServiceClient;

    @Autowired
    EnergiaServiceClient energiaServiceClient;


    public IPaymentService getPaymentService(MyContants.ServicesName serviceName) {
        if (serviceName.equals(MyContants.ServicesName.AGUA)) {
            return aguaServiceClient;
        } else if (serviceName.equals(MyContants.ServicesName.GAS)) {
            return gasServiceClient;
        } else if (serviceName.equals(MyContants.ServicesName.ENERGIA)) {
            return energiaServiceClient;
        } else if (serviceName.equals(MyContants.ServicesName.TELEFONIA)) {
            return telefoniaServiceClient;
        }

        throw new UnsupportedOperationException();
    }
}
