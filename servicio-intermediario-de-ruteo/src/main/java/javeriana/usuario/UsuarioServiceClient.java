
package javeriana.usuario;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import javeriana.MyContants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.soap.MessageFactory;

@Service
public class UsuarioServiceClient {

	@Autowired
	private EurekaClient discoveryClient;


	public long getSavings(String user) throws Exception {
		SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
		messageFactory.afterPropertiesSet();

		WebServiceTemplate webServiceTemplate = new WebServiceTemplate(messageFactory);
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("javeriana.usuario");
		marshaller.afterPropertiesSet();

		webServiceTemplate.setMarshaller(marshaller);
		webServiceTemplate.setUnmarshaller(marshaller);
		webServiceTemplate.afterPropertiesSet();

		SavingsRequest request = new SavingsRequest();
		request.setUser(user);

		SavingsResponse response = (SavingsResponse) webServiceTemplate
				.marshalSendAndReceive( getUrl() + "/savings", request);

		return response.getValue();
	}

	private String getUrl() {
		InstanceInfo instance = discoveryClient.getNextServerFromEureka(
				MyContants.ServicesName.USUARIO.getName(),
				false);

		return instance.getHomePageUrl();
	}
}
