package javeriana;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by c.contreras on 24/05/2017.
 */
@Service
public class MyEmailSender {

    private JavaMailSender javaMailSender;

    @Autowired
    public MyEmailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(String toEmail, String subject, String message) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.from","javeriana@mail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("contrerasc.javeriana@gmail.com", "javeriana123");
            }
        });
        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom();
            msg.setRecipients(Message.RecipientType.TO,
                    "javeriana@mail.com");
            msg.setSubject("Compensacion");
            msg.setText("Compensacion de factura!\n");
            Transport.send(msg);

        } catch (MessagingException mex) {
            System.out.println("send failed, exception: " + mex);
        }
    }
}