package javeriana;

/**
 * Created by chrismac on 5/23/17.
 */
public interface IPaymentService {

    public void process(String reference, long value);

    public void compensate(String reference, long value);

    public long getValue(String reference) throws PaymentServiceNotFoundException;
}
