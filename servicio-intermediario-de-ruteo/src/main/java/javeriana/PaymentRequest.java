package javeriana;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by chrismac on 5/23/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentRequest {

    private MyContants.ServicesName service;
    private String reference;
}
