
package javeriana.gas;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import javeriana.IPaymentService;
import javeriana.MyContants;
import javeriana.MyEmailSender;
import javeriana.PaymentServiceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.soap.MessageFactory;

@Service
public class GasServiceClient implements IPaymentService {

	@Autowired
	private EurekaClient discoveryClient;

	@Autowired
	private MyEmailSender emailSender;


	private String getUrl() {
		InstanceInfo instance = discoveryClient.getNextServerFromEureka(
				MyContants.ServicesName.GAS.getName(),
				false);

		return instance.getHomePageUrl();
	}

	@Override
	public void process(String reference, long value) {
		try {
			SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
			messageFactory.afterPropertiesSet();

			WebServiceTemplate webServiceTemplate = new WebServiceTemplate(messageFactory);
			Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
			marshaller.setContextPath("javeriana.gas");
			marshaller.afterPropertiesSet();

			webServiceTemplate.setMarshaller(marshaller);
			webServiceTemplate.setUnmarshaller(marshaller);
			webServiceTemplate.afterPropertiesSet();

			PaymentRequest request = new PaymentRequest();
			request.setPaymentReference(Long.parseLong(reference));
			request.setValue(value);

			String url = getUrl() + "ws";

			webServiceTemplate.marshalSendAndReceive(url, request);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void compensate(String reference, long value) {
		emailSender.sendMail("contrerasc.javeriana@gmail.com",
				"Compensacion factura " + reference + " con valor " + value,
				"Compensacion factura " + reference + " con valor " + value);
	}

	@Override
	public long getValue(String reference) throws PaymentServiceNotFoundException {
		try {
			SaajSoapMessageFactory messageFactory = new SaajSoapMessageFactory(MessageFactory.newInstance());
			messageFactory.afterPropertiesSet();

			WebServiceTemplate webServiceTemplate = new WebServiceTemplate(messageFactory);
			Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
			marshaller.setContextPath("javeriana.gas");
			marshaller.afterPropertiesSet();

			webServiceTemplate.setMarshaller(marshaller);
			webServiceTemplate.setUnmarshaller(marshaller);
			webServiceTemplate.afterPropertiesSet();

			InfoRequest request = new InfoRequest();
			request.setPaymentReference(Long.parseLong(reference));

			String url = getUrl() + "ws";

			InfoResponse response = (InfoResponse) webServiceTemplate
					.marshalSendAndReceive(url, request);

			return response.getValue();

		} catch (Exception e) {
			throw new PaymentServiceNotFoundException("something went wrong! :o", e);
		}
	}
}
