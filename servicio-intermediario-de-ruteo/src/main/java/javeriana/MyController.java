package javeriana;

import javeriana.usuario.UsuarioServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by chrismac on 5/23/17.
 */
@RestController
public class MyController {

    @Autowired
    PaymentServiceFactory factory;

    @Autowired
    UsuarioServiceClient usuarioServiceClient;


    @PreAuthorize("#oauth2.hasScope('taller')")
    @PostMapping("/pagos")
    public ResponseEntity pay(@RequestBody PaymentRequest paymentRequest, OAuth2Authentication principal) throws Exception {
        IPaymentService paymentService = factory.getPaymentService(paymentRequest.getService());

        long value = 0;
        try {
            value = paymentService.getValue(paymentRequest.getReference());
        } catch (PaymentServiceNotFoundException e) {
            return new ResponseEntity<PaymentResponse>(
                    new PaymentResponse("factura invalida"),
                    HttpStatus.BAD_REQUEST);
        }

        long savings = usuarioServiceClient.getSavings(principal.getUserAuthentication().getPrincipal().toString());

        if (savings < value) {
            return new ResponseEntity<PaymentResponse>(
                    new PaymentResponse("oops! no tienes dinero suficiente. Saldo: " + savings +
                            ", valor factura: " + value),
                    HttpStatus.BAD_REQUEST);
        }

        paymentService.process(paymentRequest.getReference(), value);

        return new ResponseEntity<PaymentResponse>(
                new PaymentResponse("factura pagada con exito por valor de " + value),
                HttpStatus.OK);
    }

    @PreAuthorize("#oauth2.hasScope('taller')")
    @DeleteMapping("/pagos")
    public ResponseEntity compensate(@RequestBody PaymentRequest paymentRequest, OAuth2Authentication principal) throws Exception {
        IPaymentService paymentService = factory.getPaymentService(paymentRequest.getService());

        long value = 0;
        try {
            value = paymentService.getValue(paymentRequest.getReference());
        } catch (PaymentServiceNotFoundException e) {
            return new ResponseEntity<PaymentResponse>(
                    new PaymentResponse("factura invalida"),
                    HttpStatus.BAD_REQUEST);
        }

        paymentService.compensate(paymentRequest.getReference(), value);

        return new ResponseEntity<PaymentResponse>(
                new PaymentResponse("factura compensada con exito por valor de " + value),
                HttpStatus.OK);
    }

}
