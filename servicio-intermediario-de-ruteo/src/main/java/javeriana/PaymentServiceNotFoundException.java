package javeriana;

/**
 * Created by chrismac on 5/23/17.
 */
public class PaymentServiceNotFoundException extends Exception {

    public PaymentServiceNotFoundException() {
        super();
    }

    public PaymentServiceNotFoundException(String message) {
        super(message);
    }

    public PaymentServiceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
