package javeriana.energia;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import javeriana.IPaymentService;
import javeriana.MyContants;
import javeriana.PaymentServiceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by chrismac on 5/21/17.
 */
@Service
public class EnergiaServiceClient implements IPaymentService {

    @Autowired
    private EurekaClient discoveryClient;

    @Autowired
    RestTemplate restTemplate;


    private String getUrl() {
        InstanceInfo instance = discoveryClient.getNextServerFromEureka(
                MyContants.ServicesName.ENERGIA.getName(),
                false);

        return instance.getHomePageUrl();
    }


    @Override
    public void process(String reference, long value) {
        CustomPaymentRequest request = new CustomPaymentRequest(Long.parseLong(reference), value);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CustomPaymentRequest> entity = new HttpEntity<>(request, headers);

        try {
            this.restTemplate.postForEntity(getUrl() + "/payment/", entity, Void.class);

        } catch (HttpClientErrorException e) {
            throw new RuntimeException("Energia failed badly and unexpectedly!");
        }
    }

    @Override
    public void compensate(String reference, long value) {
        CustomPaymentRequest request = new CustomPaymentRequest(Long.parseLong(reference), value);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CustomPaymentRequest> entity = new HttpEntity<>(request, headers);

        try {
            this.restTemplate
                    .exchange(getUrl() + "/payment/",
                            HttpMethod.DELETE,
                            entity,
                            Void.class);

        } catch (HttpClientErrorException e) {
            throw new RuntimeException("Agua failed badly and unexpectedly!");
        }
    }

    @Override
    public long getValue(String reference) throws PaymentServiceNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(headers);
        String url = getUrl() + "/payment/" + reference;

        try {
            ResponseEntity<CustomPaymentRequest> response = restTemplate
                    .exchange(url,
                            HttpMethod.GET,
                            entity,
                            CustomPaymentRequest.class);

            return response.getBody().getValue();

        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new PaymentServiceNotFoundException("");
            } else {
                throw new RuntimeException("Energia failed badly!");
            }
        }
    }


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class CustomPaymentRequest {
        private Long paymentReference;
        private Long value;
    }
}
