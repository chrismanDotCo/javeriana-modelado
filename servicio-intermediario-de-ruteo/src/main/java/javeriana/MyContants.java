package javeriana;

/**
 * Created by chrismac on 5/23/17.
 */
public class MyContants {

    public enum ServicesName {
        AGUA("servicio-pagos-agua"),
        ENERGIA("servicio-pagos-gas"),
        GAS("servicio-pagos-gas"),
        TELEFONIA("servicio-pagos-telefonia"),
        USUARIO("servicio-cuenta-usuario");

        private String name;

        ServicesName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
